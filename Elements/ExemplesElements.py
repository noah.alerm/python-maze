# Noah Alerm
# 06/05/2021

# FUNCTIONS
def same_list(llista1, llista2):
    print('\nLlista 1: ', llista1)
    print('Llista 2: ', llista2)
    print()

    are_equal = False

    i = 0
    while i < len(llista1):
        if llista1[i] == llista2[i]:
            are_equal = True
        else:
            are_equal = False
        i = i+1

    # OUTPUT
    if len(llista1) == len(llista2) and are_equal:
        return True
    else:
        return False


def matrix_tuple(matrix):
    print('\nMatriu: ')
    for i in range(len(matrix)):
        print(matrix[i])

    print()

    row_additions = []
    column_additions = []

    x = len(matrix)
    y = len(matrix[0])

    for i in range(x):
        addition_r = 0
        addition_c = 0

        for j in range(y):
            addition_r += matrix[i][j]
            addition_c += matrix[j][i]

        row_additions.append(addition_r)
        column_additions.append(addition_c)

    # OUTPUT
    print('En aquesta tupla podem veure una llista amb la suma de cada fila i una altra amb la suma '
          'da cada columna respectivament:')
    return row_additions, column_additions


def prime_or_not(num):
    is_prime = True

    if num > 1:
        for i in range(2, num):
            if num % i == 0:
                is_prime = False
                break

    # OUTPUT
    return is_prime


def multiples(num):
    x = 0

    while True:
        yield num * x
        x += 1


# DATA GETTERS
def get_func1_data():
    # Llista 1
    n = int(input('Quants elements vols introduir en la primera llista?\n'))
    llista1 = []

    print('Introdueix els elements de la primera llista:')
    while n > 0:
        llista1.append(input())
        n = n - 1

    # Llista 2
    num = int(input('Quants elements vols introduir en la segona llista?\n'))
    llista2 = []

    print('Introdueix els elements de la segona llista:')
    while num > 0:
        llista2.append(input())
        num = num - 1

    # OUTPUT
    comparator = same_list(llista1, llista2)

    if comparator:
        print('Les llistes són iguals')
    else:
        print('Les llistes són diferents')
    print()


def get_func2_data():
    # Matrix
    width = int(input('Introdueix l\'amplada de la matriu: '))
    height = int(input('Introdueix l\'altura de la matriu: '))

    matrix = []
    print('Introdueix els enters de la matriu:')

    # Lists
    while len(matrix) < height:
        llista = []
        while len(llista) < width:
            llista.append(int(input()))

        matrix.append(llista)

    # OUTPUT
    print(matrix_tuple(matrix))
    print()


def get_func3_data():
    num = int(input('Introdueix un nombre enter: \n'))
    print()

    # OUTPUT
    is_prime = prime_or_not(num)

    if is_prime:
        print('El número és primer\n')
    else:
        print('El número no és primer\n')


def get_func4_data():
    num = float(input('Introdueix un nombre enter: \n'))
    print()

    # OUTPUT
    print('A coninuació, es printaran tots els múltiples de ', num, ':')
    for i in multiples(num):
        print(i)


# MENU
def menu():
    while True:
        print('**********************************')
        print('               Menu               ')
        print('**********************************')
        print('Quina funció vols mostrar?')
        print('1 - Comparador de llistes')
        print('2 - Manipulador de matrius')
        print('3 - Detector de nombres primers')
        print('4 - Generador de múltiples')
        print('0 - EXIT')

        num = int(input())
        if num == 0:
            break
        if num == 1:
            get_func1_data()
        if num == 2:
            get_func2_data()
        if num == 3:
            get_func3_data()
        if num == 4:
            get_func4_data()


# Main
menu()
