# Noah Alerm
# 11/03/2021

# Mostra el valor absolut d'un nombre.
def exemple_abs():
    print('\nMètode abs():\n')
    print('abs(10): ', abs(10))
    print('abs(-10): ', abs(-10))
    print()


# Mostra True si els elements d'un iterable són true o l'iterable està buit.
def exemple_all():
    print('\nMètode all():\n')
    llista1 = ['a', 'b', 'c']
    print('Llista 1: ', llista1)
    print('all(llista1): ', all(llista1))
    dicc = {0, 1, 0}
    print('Diccionari: ', dicc)
    print('all(dicc): ', all(dicc))
    llista2 = []
    print('Llista 2: ', llista2)
    print('all(llista2): ', all(llista2))
    print()


# # Mostra True si els elements d'un iterable són true  i False si l'iterable està buit.
def exemple_any():
    print('\nMètode any():\n')
    llista1 = ['a', 'b', 'c']
    print('Llista 1: ', llista1)
    print('any(llista1): ', any(llista1))
    dicc = {0, 1, 0}
    print('Diccionari: ', dicc)
    print('any(dicc): ', any(dicc))
    llista2 = []
    print('Llista 2: ', llista2)
    print('any(llista2): ', any(llista2))
    print()


# Mostra la representació binària d'un enter
def exemple_bin():
    print('\nMètode bin():\n')
    print('bin(10): ', bin(10))
    print()


# Retorna True si l'element no és ni False, ni buit, ni None, ni 0
def exemple_bool():
    print('\nMètode bool():\n')
    print('bool(0): ', bool(0))
    print('bool(1): ', bool(1))
    print('bool([]): ', bool([]))
    print('bool([1, 2]): ', bool([1, 2]))
    print()


# Mostra el caràcter ASCII corresponent a aquest nombre
def exemple_chr():
    print('\nMètode chr():\n')
    print('chr(33): ', chr(33))
    print('chr(36): ', chr(36))
    print()


# Retorna el quocient i el residu del primer element entre el segon
def exemple_divmod():
    print('\nMètode divmod():\n')
    print('divmod(10, 2): ', divmod(10, 2))
    print()


# Retorna una tupla de la llista amb els elements enumerats
def exemple_enumerate():
    print('\nMètode enumerate():\n')
    llista = ['a', 'b', 'c']
    print('Llista: ', llista)
    print('list(enumerate(llista)): ', list(enumerate(llista)))
    print()


# Analitza una expressió passada per paràmetre
def exemple_eval():
    print('\nMètode eval():\n')
    x = 1
    print('x: ', x)
    print('eval(x + 1): ', eval('x + 1'))
    print()


def filter_list(llista):
    vocals = ['a', 'e', 'i', 'o', 'u']

    if llista in vocals:
        return True
    else:
        return False


# Crea una llista dels elements d'un iterable que fan que una funció retorni True
def exemple_filter():
    print('\nMètode filter():\n')
    llista = ['a', 'b', 'c', 'd', 'e']
    print('llista: ', llista)
    print('list(filter(filter_list, llista)): ', list(filter(filter_list, llista)))
    print()


# Retorna el màxim d'un grup de valors o d'un iterable
def exemple_max():
    print('\nMètode max():\n')
    print('max(1, 5, 4, 6, 3): ', max(1, 5, 4, 6, 3))
    print()


# Retorna el mínim d'un grup de valors o d'un iterable
def exemple_min():
    print('\nMètode min():\n')
    print('min(1, 5, 4, 6, 3): ', min(1, 5, 4, 6, 3))
    print()


# Retorna el resultat de la potència dels números passats per paràmetre
def exemple_pow():
    print('\nMètode pow():\n')
    print('pow(2, 3): ', pow(2, 3))
    print()


# Retorna una llista passada per paràmetre del revés
def exemple_reversed():
    print('\nMètode reversed():\n')
    llista = ['a', 'b', 'c']
    print('Llista: ', llista)
    print('list(reversed(llista)): ', list(reversed(llista)))
    print('llista', llista)
    print()


# Arrodoneix el nombre
def exemple_round():
    print('\nMètode round():\n')
    print('round(2.35): ', round(2.35))
    print('round(2.35, 1): ', round(2.35, 1))
    print()


# Convereix el paràmetre en String
def exemple_str():
    print('\nMètode str():\n')
    print('str(2.35): ', str(2.35))
    print()


# Suma els valors d'un iterable
def exemple_sum():
    print('\nMètode sum():\n')
    llista = [1, 5, 4, 6, 3]
    print('Llista: ', llista)
    print('sum(llista): ', sum(llista))
    print()


# Mostra el tipus de l'objecte introduït per paràmetre.
def exemple_type():
    print('\nMètode type():\n')
    llista = [1, 5, 4, 6, 3]
    print('Llista: ', llista)
    print('type(llista): ', type(llista))
    print()


def menu():
    while True:
        print('**************************')
        print('          Menu            ')
        print('**************************')
        print('Quin mètode vols mostrar?')
        print('1 - abs()')
        print('2 - all()')
        print('3 - any()')
        print('4 - bin()')
        print('5 - bool()')
        print('6 - chr()')
        print('7 - divmod()')
        print('8 - enumerate()')
        print('9 - eval()')
        print('10 - filter()')
        print('11 - max()')
        print('12 - min()')
        print('13 - pow()')
        print('14 - reversed()')
        print('15 - round()')
        print('16 - str()')
        print('17 - sum()')
        print('18 - type()')
        print('0 - EXIT')

        num = int(input())
        if num == 0:
            break
        if num == 1:
            exemple_abs()
        if num == 2:
            exemple_all()
        if num == 3:
            exemple_any()
        if num == 4:
            exemple_bin()
        if num == 5:
            exemple_bool()
        if num == 6:
            exemple_chr()
        if num == 7:
            exemple_divmod()
        if num == 8:
            exemple_enumerate()
        if num == 9:
            exemple_eval()
        if num == 10:
            exemple_filter()
        if num == 11:
            exemple_max()
        if num == 12:
            exemple_min()
        if num == 13:
            exemple_pow()
        if num == 14:
            exemple_reversed()
        if num == 15:
            exemple_round()
        if num == 16:
            exemple_str()
        if num == 17:
            exemple_sum()
        if num == 18:
            exemple_type()


# Main
menu()
