# Noah Alerm
# 25/03/2021
import math


def addition(numbers):
    result = 0

    for i in numbers:
        result = result + i

    print(result)


def subtraction(numbers):
    result = numbers[0]

    i = 1
    while i < len(numbers):
        result = result - numbers[i]
        i = i + 1

    print(result)


def multiplication(numbers):
    result = numbers[0]

    i = 1
    while i < len(numbers):
        result = result * numbers[i]
        i = i + 1

    print(result)


def division(numbers):
    result = numbers[0]

    i = 1
    while i < len(numbers):
        result = result / numbers[i]
        i = i + 1

    print(result)


def basic_op_menu():
    while True:
        print('*************************************************')
        print('************** Operacions bàsiques **************')
        print('*************************************************')
        print('1 - Suma')
        print('2 - Resta')
        print('3 - Multiplicació')
        print('4 - Divisió')
        print('0 - EXIT')

        print('\nQuina operació vols realitzar?')
        num = int(input())
        print()

        if num == 0:
            break
        if num == 1:
            print('Quants nombres vols sumar?')
            values = float(input())
            numbers = []
            print('Introdueix els valors que vols sumar: ')
            while values > 0:
                numbers.append(float(input()))
                values = values - 1

            addition(numbers)
        if num == 2:
            print('Quants nombres vols restar?')
            values = float(input())
            numbers = []
            print('Introdueix els valors que vols restar: ')
            while values > 0:
                numbers.append(float(input()))
                values = values - 1

            subtraction(numbers)
        if num == 3:
            print('Quants nombres vols multiplicar?')
            values = float(input())
            numbers = []
            print('Introdueix els valors que vols multiplicar: ')
            while values > 0:
                numbers.append(float(input()))
                values = values - 1

            multiplication(numbers)
        if num == 4:
            print('Quants nombres vols multiplicar?')
            values = float(input())
            numbers = []
            print('Introdueix els valors que vols multiplicar: ')
            while values > 0:
                numbers.append(float(input()))
                values = values - 1

            division(numbers)


def sin(number):
    print(math.sin(math.radians(number)))


def cos(number):
    print(math.cos(math.radians(number)))


def tan(number):
    print(math.tan(math.radians(number)))


def trigonometry_op_menu():
    while True:
        print('*************************************************')
        print('****************** Trigonometria ****************')
        print('*************************************************')
        print('1 - Sinus')
        print('2 - Cosinus')
        print('3 - Tangent')
        print('0 - EXIT')

        print('\nQuina operació vols realitzar?')
        num = int(input())
        print()

        if num == 0:
            break
        if num == 1:
            value = float(input('Introdueix el valor del qual vulguis calcular el sinus: '))
            sin(value)
        if num == 2:
            value = float(input('Introdueix el valor del qual vulguis calcular el cosinus: '))
            cos(value)
        if num == 3:
            value = float(input('Introdueix el valor del qual vulguis calcular la tangent: '))
            tan(value)


def root(value, exp):
    print(value ** (1/exp))


def power_and_root_op_menu():
    while True:
        print('*************************************************')
        print('*************** Potències i arrels **************')
        print('*************************************************')
        print('1 - Potència')
        print('2 - Arrel quadrada')
        print('3 - Altres arrels')
        print('0 - EXIT')

        print('\nQuina operació vols realitzar?')
        num = int(input())
        print()

        if num == 0:
            break
        if num == 1:
            base = float(input('Introdueix el valor del qual vulguis calcular la potència: '))
            exp = float(input('Introdueix el valor de l\'exponent: '))
            print(pow(base, exp))
        if num == 2:
            value = float(input('Introdueix el valor del qual vulguis calcular l\'arrel quadrada: '))
            print(math.sqrt(value))
        if num == 3:
            value = int(input('Introdueix el valor del qual vulguis calcular l\'arrel: '))
            exp = float(input('Introdueix el tipus d\'arrel que és: '))
            root(value, exp)


def log_op_menu():
    while True:
        print('*************************************************')
        print('******************* Logarismes ******************')
        print('*************************************************')
        print('1 - Log de a')
        print('2 - Log')
        print('3 - Logarisme neperià')
        print('0 - EXIT')

        print('\nQuina operació vols realitzar?')
        num = int(input())
        print()

        if num == 0:
            break
        if num == 1:
            value = int(input('Introdueix el valor del qual vulguis calcular el logarisme: '))
            base = float(input('Introdueix la base del logarisme: '))
            print(math.log(value, base))
        if num == 2:
            value = int(input('Introdueix el valor del qual vulguis calcular el logarisme: '))
            print(math.log(value, 10))
        if num == 3:
            value = int(input('Introdueix el valor del qual vulguis calcular el logarisme: '))
            print(math.log(value, math.e))


def advanced_op_menu():
    while True:
        print('*************************************************')
        print('************** Operacions avançades *************')
        print('*************************************************')
        print('1 - Trigonometria')
        print('2 - Potències i arrels')
        print('3 - Logarismes')
        print('0 - EXIT')

        print('\nQuin tipus d\'operacions vols realitzar?')
        num = int(input())
        print()

        if num == 0:
            break
        if num == 1:
            trigonometry_op_menu()
        if num == 2:
            power_and_root_op_menu()


def main_menu():
    while True:
        print('*************************************************')
        print('****************** CALCULADORA ******************')
        print('*************************************************')
        print('1 - Operacions bàsiques')
        print('2 - Operacions avançades')
        print('0 - EXIT')

        print('\nQuin tipus d\'operacions vols realitzar?')
        num = int(input())
        print()

        if num == 0:
            break
        if num == 1:
            basic_op_menu()
        if num == 2:
            advanced_op_menu()


# Main
main_menu()
