# Noah Alerm
# 06/05/2021

# DISCLAIMER 1: The player doesn't always reach the end!
# DISCLAIMER 2: The recommended maze size to avoid errors is 10x10.

# DESCRIPTION: The player moves towards the end of the maze using AI (Artificial Intelligence) and avoiding
# the obstacles (True). Mazes are generated randomly and 1 out of 4 boxes is an obstacle. The player might end
# up stuck between obstacles or moving in a loop. In this case, the player never reaches the end of the maze, which
# means the game is over.

import random


class Position:
    # CONSTRUCTOR
    def __init__(self, pos_x, pos_y):
        self.pos_x = pos_x
        self.pos_y = pos_y

    # EQUALS
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.pos_x == other.pos_x and self.pos_y == other.pos_y
        else:
            return False


class Player:
    # CONSTRUCTOR
    def __init__(self, maze):
        self.pos = Position(0, maze.start_y)
        self.maze = maze

    # POSSIBLE MOVES
    def is_move_possible(self, new_pos):
        if 0 <= new_pos.pos_x < self.maze.width and 0 <= new_pos.pos_y < self.maze.height:
            # A move is possible if the value of the new position isn't 'True' and this position is 1 box away
            # (vertically, horizontally or diagonally) from the current position.
            if (not self.maze.matrix[new_pos.pos_x][new_pos.pos_y] or
                self.maze.matrix[new_pos.pos_x][new_pos.pos_y] == 'Start' or
                self.maze.matrix[new_pos.pos_x][new_pos.pos_y] == 'End') and \
                    ((new_pos.pos_x == self.pos.pos_x or abs(new_pos.pos_x - self.pos.pos_x) == 1) and
                     (new_pos.pos_y == self.pos.pos_y or abs(new_pos.pos_y - self.pos.pos_y) == 1)):
                return True
            else:
                return False
        else:
            return False

    # CLOSEST MOVE TO END
    def get_closest_to_end(self):
        # Possible new positions
        positions = [Position(self.pos.pos_x, self.pos.pos_y), Position(self.pos.pos_x + 1, self.pos.pos_y),
                     Position(self.pos.pos_x - 1, self.pos.pos_y), Position(self.pos.pos_x, self.pos.pos_y + 1),
                     Position(self.pos.pos_x, self.pos.pos_y - 1), Position(self.pos.pos_x + 1, self.pos.pos_y + 1),
                     Position(self.pos.pos_x + 1, self.pos.pos_y - 1), Position(self.pos.pos_x - 1, self.pos.pos_y + 1),
                     Position(self.pos.pos_x - 1, self.pos.pos_y - 1)]

        # End position
        end = Position(self.maze.height - 1, self.maze.end_y)

        closest = positions[0]

        for i in positions:
            # New position must be inside the maze
            if self.maze.height > i.pos_x >= 0 and self.maze.width > i.pos_y >= 0:
                # Move must be possible
                if self.is_move_possible(i):
                    row_diff = abs(end.pos_x - i.pos_x)
                    col_diff = abs(end.pos_y - i.pos_y)

                    # Closest Position Check
                    if row_diff + col_diff < (abs(end.pos_x - closest.pos_x) + abs(end.pos_y - closest.pos_y)):
                        closest = i

        return closest

    # AI MOVEMENT
    def move(self):
        new_pos = self.get_closest_to_end()

        # If the closest position is the current position, a new position is chosen randomly.
        if new_pos == self.pos:
            i = random.randint(self.pos.pos_x - 1, self.pos.pos_x + 1)
            j = random.randint(self.pos.pos_y - 1, self.pos.pos_y + 1)

            new_pos = Position(i, j)

        # MOVEMENT
        if self.is_move_possible(new_pos):
            # Current position's new value
            if self.pos.pos_y == self.maze.start_y and self.pos.pos_x == 0:
                self.maze.matrix[self.pos.pos_x][self.pos.pos_y] = 'Start'
            else:
                self.maze.matrix[self.pos.pos_x][self.pos.pos_y] = False
            # Move
            self.pos = new_pos
        else:
            self.move()


# noinspection PyTypeChecker
class Maze:
    # CONSTRUCTOR
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.matrix = [[False for _ in range(width)] for _ in range(height)]
        self.start_y = random.randint(0, self.height - 1)
        self.end_y = random.randint(0, self.height - 1)

        # START CREATION
        self.matrix[0][self.start_y] = 'Start'

        # END CREATION
        self.matrix[self.width - 1][self.end_y] = 'End'

        # OBSTACLE CREATION
        obstacle_num = self.width * self.height / 4

        while obstacle_num > 0:
            x = random.randint(0, self.width - 1)
            y = random.randint(0, self.height - 1)

            if not self.matrix[x][y] and self.matrix[x][y] != 'Start' and self.matrix[x][y] != 'End' \
                    and self.matrix[x][y] != 'Player':
                self.matrix[x][y] = True
                obstacle_num -= 1

    # PRINT MAZE
    def print(self):
        for i in range(self.width):
            for j in range(self.height):
                # False
                if not self.matrix[i][j]:
                    print(self.matrix[i][j], end='')
                # True
                if self.matrix[i][j] and self.matrix[i][j] != 'Start' and self.matrix[i][j] != 'End' \
                        and self.matrix[i][j] != 'Player':
                    print('\033[94m', end='')
                    print(self.matrix[i][j], end='\033[0m')
                # Start
                if self.matrix[i][j] == 'Start':
                    print('\033[93m', end='')
                    print(self.matrix[i][j], end='\033[0m')
                # End
                if self.matrix[i][j] == 'End':
                    print('\033[91m', end='')
                    print(self.matrix[i][j], end='\033[0m')
                # Player
                if self.matrix[i][j] == 'Player':
                    print('\033[95m', end='')
                    print(self.matrix[i][j], end='\033[0m')
            print()

    # PRINT PLAYER
    def print_player(self, pos):
        self.matrix[pos.pos_x][pos.pos_y] = 'Player'


# GAME OVER
def game_over(pos, a, b):
    if pos.pos_x == a and pos.pos_y == b:
        return True
    else:
        return False


# GAME
def game():
    # Maze Size
    width = int(input('Enter the width of the maze: '))
    height = int(input('Enter the height of the maze: '))

    moves_counter = 0

    maze = Maze(width, height)
    player = Player(maze)

    # LOOP
    while True:
        maze.print_player(player.pos)
        maze.print()

        # GAME OVER
        if game_over(player.pos, maze.height - 1, maze.end_y):
            print('VICTORY!')
            print('The player has exited the maze in ', moves_counter, ' moves')
            break

        player.move()

        if input('\nPress ENTER to see next move or enter END to end the game:\n') == 'END':
            print('GAME OVER')
            print('The player has abandoned the maze after ', moves_counter, ' moves')
            break
        moves_counter += 1


# Main
game()
