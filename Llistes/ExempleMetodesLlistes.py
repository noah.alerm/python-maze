# Noah Alerm
# 11/02/2021

# Afegeix un element al final de la llista
def exemple_append():
    print('Mètode append("d"):')
    llista = ['a', 'b', 'c']
    print(llista)
    llista.append('d')
    print(llista)


# Afegeix un element en una posició determinada de la llista
def exemple_insert():
    print('\nMètode insert(0,"d"):')
    llista = ['a', 'b', 'c']
    print(llista)
    llista.insert(0, 'd')
    print(llista)


# Esborra un element de la llista
def exemple_remove():
    print('\nMètode remove("c"):')
    llista = ['a', 'b', 'c']
    print(llista)
    llista.remove('c')
    print(llista)


# Ordena la llista
def exemple_sort():
    print('\nMètode sort():')
    llista = ['b', 'c', 'a']
    print(llista)
    llista.sort()
    print(llista)


# Copia una llista de manera ordenada
def exemple_sorted():
    print('\nMètode sorted(llista1):')
    llista1 = ['c', 'e', 'd', 'a', 'b']
    print('Llista 1: ', llista1)
    llista2 = sorted(llista1)
    print('Llista 2:', llista2)


# Inverteix els valors de la llista
def exemple_reverse():
    print('\nMètode reverse():')
    llista = ['a', 'b', 'c']
    print(llista)
    llista.reverse()
    print(llista)


# Esborra l'element que es troba a una posició determinada de la llista
def exemple_pop():
    print('\nMètode pop(1):')
    llista = ['a', 'b', 'c']
    print(llista)
    print('Element eliminat: ', llista.pop(1))
    print(llista)


# Amplia una llista amb una altra llista determinada
def exemple_extend():
    print('\nMètode extend(llista2):')
    llista1 = ['a', 'b', 'c']
    llista2 = ['d', 'e', 'f']
    print('Llista 1: ', llista1)
    print('Llista 2: ', llista2)
    llista1.extend(llista2)
    print(llista1)


# Mostra la quantitat de vegades que es repeteix un element determinat de la llista
def exemple_count():
    print('\nMètode count("a"):')
    llista = ['a', 'b', 'c', 'a']
    print(llista)
    count = llista.count('a')
    print('Count: ', count)


# Mostra la posició de la primera vegada que apareix un element determinat a la llista
def exemple_index():
    print('\nMètode index("b"):')
    llista = ['a', 'b', 'c', 'b']
    print(llista)
    index = llista.index('b')
    print('i: ', index)


# Buida la llista
def exemple_clear():
    print('\nMètode clear():')
    llista = ['a', 'b', 'c']
    print(llista)
    llista.clear()
    print(llista)


# Copia llistes
def exemple_copy():
    print('\nMètode copy():')
    llista1 = ['a', 'b', 'c']
    print('Llista 1: ', llista1)
    llista2 = llista1.copy()
    print('Llista 2: ', llista2)


def exemples():
    exemple_append()
    exemple_insert()
    exemple_remove()
    exemple_sort()
    exemple_sorted()
    exemple_reverse()
    exemple_pop()
    exemple_extend()
    exemple_count()
    exemple_index()
    exemple_clear()
    exemple_copy()


# Main
exemples()
