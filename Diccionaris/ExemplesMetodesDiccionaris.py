# Noah Alerm
# 18/02/2021

# Mostra una llista de les claus i els valors
def exemple_items():
    print('\nMètode items():')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    llista = dicc.items()
    print(llista)
    print()


# Mostra una llista de les claus
def exemple_keys():
    print('\nMètode keys():')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    llista = dicc.keys()
    print(llista)
    print()


# Mostra una llista dels valors
def exemple_values():
    print('\nMètode values():')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    llista = dicc.values()
    print(llista)
    print()


# Copia un diccionari
def exemple_copy():
    print('\nMètode copy():')
    dicc1 = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print('Diccionari 1: ', dicc1)
    dicc2 = dicc1.copy()
    print('Diccionari 2: ', dicc2)
    print()


# Esborra un diccionari
def exemple_clear():
    print('\nMètode clear():')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    dicc.clear()
    print(dicc)
    print()


# Mostra un valor a partir de la seva clau
def exemple_get():
    print('\nMètode get("IDE"), get("Professor"), get("IDE", "x") i get("Professor", "x"):')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    print('get("IDE"): ', dicc.get('IDE'))
    print('get("Professor"): ', dicc.get('Professor'))
    print('get("IDE", "x"): ', dicc.get('IDE', 'x'))
    print('get("Professor", "x"): ', dicc.get('Professor', 'x'))
    print()


# Esborra un element del diccionari a partir de la clau i mostra el valor
def exemple_pop():
    print('\nMètode pop("IDE"):')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    print('Element eliminat: ', dicc.pop('IDE'))
    print(dicc)
    print()


# Elimina un dels elements i el mostra
def exemple_popitem():
    print('\nMètode popitem():')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    print('Element eliminat: ', dicc.popitem())
    print(dicc)
    print()


# Crea un diccionari a partir d'un conjunt de claus i un valor
def exemple_fromkeys():
    print('\nMètode fromkeys(keys, value):')
    keys = {'a', 'e', 'i', 'o', 'u'}
    print('Keys: ', keys)
    value = 'vocal'
    print('Value: ', value)
    dicc = dict.fromkeys(keys, value)
    print('Diccionari: ', dicc)
    print()


# Crea una clau si aquesta no existeix i la insereix al diccionari amb un valor per defecte
def exemple_setdefault():
    print('\nMètode setdefault("Codificació", "UTF-8"):')
    dicc = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print(dicc)
    dicc.setdefault('Codificació', 'UTF-8')
    print(dicc)
    print()


# Afegeix els elements d'un diccionari a un altre diccionari
def exemple_update():
    print('\nMètode update(dicc2):')
    dicc1 = {'Llenguatge': 'python', 'IDE': 'PyCharm'}
    print('Diccionari 1: ', dicc1)
    dicc2 = {'Codificació': 'UTF-8'}
    print('Diccionari 2:', dicc2)
    dicc1.update(dicc2)
    print('Diccionari 1: ', dicc1)
    print()


def menu():
    while True:
        print('Quin mètode vols mostrar?')
        print('1 - items()')
        print('2 - keys()')
        print('3 - values()')
        print('4 - copy()')
        print('5 - clear()')
        print('6 - get()')
        print('7 - pop()')
        print('8 - popitem()')
        print('9 - fromkeys()')
        print('10 - setdefault()')
        print('11 - update()')
        print('0 - EXIT')
        num = int(input())
        if num == 0:
            break
        if num == 1:
            exemple_items()
        if num == 2:
            exemple_keys()
        if num == 3:
            exemple_values()
        if num == 4:
            exemple_copy()
        if num == 5:
            exemple_clear()
        if num == 6:
            exemple_get()
        if num == 7:
            exemple_pop()
        if num == 8:
            exemple_popitem()
        if num == 9:
            exemple_fromkeys()
        if num == 10:
            exemple_setdefault()
        if num == 11:
            exemple_update()


# Main
menu()
